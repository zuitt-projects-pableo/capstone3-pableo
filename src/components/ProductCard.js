import { Card, Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default function ProductCard({ productProp }) {
  const { name, category, description, price, image, _id } = productProp;

  return (
    <Card className="col-12 col-lg-3 p-3">
      <Card.Title>
        <h4 className="text-white">{name}</h4>
      </Card.Title>
      <Card.Body>
        <div className="bg-prod rounded-3 p-3 mb-3">
          <img className="img-prod" src={`${image}`} alt="product" />
        </div>
        <h6>Category:</h6>
        <p className="text-capitalize">{category} Guitar</p>
        <h6>Price:</h6>
        <p>Php {price}</p>

        <Button className="btn-prod" as={Link} to={`/products/${_id}`}>
          See Details
        </Button>
      </Card.Body>
    </Card>
  );
}
