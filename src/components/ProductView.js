import React, {useState, useEffect, useContext} from 'react';
import {Container, Card, Button} from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ProductView(){

	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [name, setName] = useState('');
	const [category, setCategory] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");

	const {productId} = useParams();

	const buy = (productId) => {
		fetch('https://backend-capstone3-r6bk.onrender.com/users/buy', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				productId: productId,
				name : name,
				category : category,
				description : description,
				price : price,
				image : image
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true){

				Swal.fire({
					title: "Added to Cart!",
					icon: "success",
					text: "You have successfully added to cart"
				})

				history("/products");

			} else {

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
			}
		})
	}

	useEffect(() => {

		fetch(`https://backend-capstone3-r6bk.onrender.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			setName(data.name);
			setCategory(data.category);
			setDescription(data.description);
			setPrice(data.price);
			setImage(data.image);
		})
	}, [productId])

return(
	<Container className="mt-5">
		<Card className="col-12 p-3 mx-0">
			<Card.Title>
				<h4 className="text-white">{name}</h4>
			</Card.Title>
			<Card.Body>
				<div className="bg-white rounded-3 p-3 p-md-5 mb-3">
					<img className="img-prod" src={`${image}`} alt="product" />
				</div>

				<Card.Subtitle>Category</Card.Subtitle>
				<Card.Text className='text-capitalize'>{category} Guitar</Card.Text>
				
				<Card.Subtitle>Description</Card.Subtitle>
				<Card.Text>{description}</Card.Text>
				
				<Card.Subtitle>Price</Card.Subtitle>
				<Card.Text>Php {price}</Card.Text>

				<Card.Subtitle>Availability</Card.Subtitle>
				<Card.Text>In Stock</Card.Text>
				
				{ user.id !== null ?
					<Button className="btn-prod" onClick={() => buy(productId)}>Add to Cart</Button>
					:
					<Link className="btn btn-danger" to="/login">Log In to Buy</Link>
				}
			</Card.Body>
		</Card>
	</Container>

	)
}
