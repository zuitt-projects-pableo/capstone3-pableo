import React, { Fragment, useState, useEffect } from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';

export default function AdminView(props){

	// Destructure our products data from the props being passed by the parent component (products page)
	// Includes the "fetchData" function that retrieves the products from our database
	const { productData, fetchData } = props;

	// States for form inputs
	const [productId, setProductId] = useState("");
	const [products, setProducts] = useState([]);
	const [name, setName] = useState("");
	const [category, setCategory] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");

	// States to open/close modals
	const [showEdit, setShowEdit] = useState(false);
	const [showAdd, setShowAdd] = useState(false);

	// Functions to toggle the opening and closing of the "Add product" modal
	const openAdd = () => setShowAdd(true);
	const closeAdd = () => setShowAdd(false);

	/* 
	Function to open the "Edit product" modal:
		- Fetches the selected product data using the product ID
		- Populates the values of the input fields in the modal form
		- Opens the "Edit product" modal
	*/
	const openEdit = (productId) => {

		// Fetches the selected product data using the product ID
		fetch(`https://backend-capstone3-r6bk.onrender.com/products/${ productId }`)
		.then(res => res.json())
		.then(data => {

			// Changes the states for binded to the input fields
			// Populates the values of the input files in the modal form
			setProductId(data._id);
			setName(data.name);
			setCategory(data.category);
			setDescription(data.description);
			setPrice(data.price);
			setImage(data.image);
		})

		// Opens the "Edit product" modal
		setShowEdit(true);
	};

	/* 
	Function to close our "Edit product" modal:
		- Reset from states back to their initial values
		- Empties the input fields in the form whenever the modal is opened for adding a product
	*/
	const closeEdit = () => {

		setShowEdit(false);
		setName("");
		setCategory("");
		setDescription("");
		setPrice(0);
		setImage("");

	};

	const addProduct = (e) => {

		// Prevent the form from redirecting to a different page on submit 
		// Helps retain the data if adding a product is unsuccessful
		e.preventDefault()

		fetch(`https://backend-capstone3-r6bk.onrender.com/products`, {
			method: 'POST',
			headers: {
				Authorization: `Bearer ${ localStorage.getItem('token') }`,
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				name: name,
				category: category,
				description: description,
				price: price,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {

			// If the new product is successfully added
			if (data === true) {

				// Invoke the "fetchData" function passed from our parent component (products page)
				// Rerenders the page because of the "useEffect"
				fetchData();

				// Show a success message via sweet alert
				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully added."					
				})

				// Reset all states to their initial values
				// Provides better user experience by clearing all the input fieles when the user adds another product
				setName("")
				setCategory("")
				setDescription("")
				setPrice(0)
				setImage("")

				// Close the modal
				closeAdd();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				})
			}
		})
	}

	const deleteProduct = (productId) => {
			let token = localStorage.getItem('token');
			fetch(`https://backend-capstone3-r6bk.onrender.com/products/${productId}/delete`, {
				method: "DELETE",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {

			if (data !== true) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully deleted."
				});

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}

	const editProduct = (e, productId) => {
		
		e.preventDefault();

		fetch(`https://backend-capstone3-r6bk.onrender.com/products/${ productId }`, {
			method: 'PUT',
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${ localStorage.getItem('token') }`
			},
			body: JSON.stringify({
				name: name,
				category: category,
				description: description,
				price: price,
				image: image
			})
		})
		.then(res => res.json())
		.then(data => {

			if (data === true) {

				fetchData();

				Swal.fire({
					title: "Success",
					icon: "success",
					text: "Product successfully updated."
				});

				closeEdit();

			} else {

				fetchData();

				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please try again."
				});

			}

		})
	}

	// Map through the products received from the parent component (product page)
	// Re-renders the table whenever the "productsData" is updated by adding, editing and deleting a product

		const archiveToggle = (productId, isActive) => {

			fetch(`https://backend-capstone3-r6bk.onrender.com/products/${ productId }/archive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully archived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const unarchiveToggle = (productId, isActive) => {

			fetch(`https://backend-capstone3-r6bk.onrender.com/products/${ productId }/unarchive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully unarchived."
					});

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		};

useEffect(() => {

		const productsArr = productData.map(product => {

			return(

				<tr key={product._id}>
					<td>{product.name}</td>
					<td className="text-capitalize">{product.category} Guitar</td>
					<td ><div className="desc-prod-table text-truncate">{product.description}</div></td>
					<td>{product.price}</td>
					<td>
						<img className="img-prod-table" src={`${product.image}`} alt="product" />
					</td>
					<td>
						{/* 
							- If the product's "isActive" field is "true" displays "available"
							- Else if the product's "isActive" field is "false" displays "unavailable"
						*/}
						{product.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td className='text-center'>
						<Button
							variant="success"
							size="sm"
							className="me-2"
							onClick={() => openEdit(product._id)}
						>
							Update
						</Button>
						{/* 
							- Display a red "Disable" button if product is "active"
							- Else display a green "Enable" button if product is "inactive"
						*/}
						{product.isActive
							?
							<Button 
								variant="primary" 
								size="sm"
								className="me-2"
								onClick={() => archiveToggle(product._id, product.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								variant="primary"
								size="sm"
								className="me-2"
								onClick={() => unarchiveToggle(product._id, product.isActive)}
							>
								Enable
							</Button>
						}
						<Button
							variant="danger"
							size="sm"
							onClick={() => deleteProduct(product._id)}
						>
							Delete
						</Button>
					</td>
				</tr>

			)

		});

		// Set the "products" state with the table rows returned by the map function
		// Renders table row elements inside the table via this "AdminView" return statement below
		setProducts(productsArr);

	}, [productData, fetchData]);

	return(
		<Fragment>

			<div className="text-center my-4">
				<h2 className="text-white">Admin Dashboard</h2>
				<div className="d-flex justify-content-center">
					<Button className="btn-card" onClick={openAdd}>Add New Product</Button>			
				</div>			
			</div>

			<Table striped bordered hover responsive className="admin-table">
				<thead className="bg-dark text-white">
					<tr>
						<th>Name</th>
						<th>Category</th>
						<th>Description</th>
						<th>Price</th>
						<th>Image</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody className="bg-warning">
					{products}
				</tbody>
			</Table>

			{/*ADD MODAL*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton>
						<Modal.Title>Add product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Label>Category</Form.Label>
						<Form.Select value={category} onChange={e => setCategory(e.target.value)} required>
							<option>Select Category</option>
							<option value="electric">Electric Guitar</option>
							<option value="acoustic">Acoustic Guitar</option>
							<option value="bass">Bass Guitar</option>
						</Form.Select>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="textarea" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productImage">
							<Form.Label>Image</Form.Label>
							<Form.Control type="text" value={image}  onChange={e => setImage(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			{/*EDIT MODAL*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Label>Category</Form.Label>
						<Form.Select value={category} onChange={e => setCategory(e.target.value)} required>
							<option>Select Category</option>
							<option value="electric">Electric Guitar</option>
							<option value="acoustic">Acoustic Guitar</option>
							<option value="bass">Bass Guitar</option>
						</Form.Select>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control as="textarea" rows={3} value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productImage">
							<Form.Label>Image</Form.Label>
							<Form.Control type="text" value={image}  onChange={e => setImage(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Fragment>
	)
}


