import React, { Fragment, useState, useEffect } from "react";
import { Row, Col, Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import img1 from "../images/bg.png";
import img2 from "../images/bg2.png";
import img3 from "../images/bg3.png";

export default function Home() {
  const images = [img1, img2, img3];
  const [currentImage, setCurrentImage] = useState(0);
  const [imageVisible, setImageVisible] = useState(false);

  useEffect(() => {
    const imageInterval = setInterval(() => {
      setImageVisible(false);
      setTimeout(() => {
        setCurrentImage((prevImage) => (prevImage + 1) % images.length);
        setImageVisible(true);
      }, 500);
    }, 3000);

    return () => {
      clearInterval(imageInterval);
    };
  }, []);

  return (
    <Fragment>
      <div className="circle"></div>
      <Row className="justify-content-center align-items-center">
        <Col className="col-12 col-md-7 p-5 order-2 order-md-1">
          <h1 className="text-white display-4 fw-bold">String Files</h1>
          <p className="text-white">
            String Files aims to provide the best guitars at a competitve price
            online. We're always on the lookout for the greatest items to
            satisfy today's savvy guitarists. We have the guitars that are
            appropriate for you, whether you are a professional or a hobbyist.
          </p>

          <Button className="btn-home" as={Link} to="/products">
            Shop Now!
          </Button>
        </Col>
        <Col className="col-8 col-md-5 order-1 order-md-2 home-guitar mx-auto">
          <img
            src={images[currentImage]}
            className={`img-fluid shadow-4 ${imageVisible ? "fade-in" : ""}`}
            alt="..."
          />
        </Col>
      </Row>

      <div className="page-content">
        <div className="cardP">
          <div className="content">
            <h2 className="title">Acoustic Guitar</h2>
            <Button
              className="btn-card"
              as={Link}
              to="/products/category/acoustic"
            >
              Buy Now
            </Button>
          </div>
        </div>
        <div className="cardP">
          <div className="content">
            <h2 className="title">Electric Guitar</h2>
            <Button
              className="btn-card"
              as={Link}
              to="/products/category/electric"
            >
              Buy Now
            </Button>
          </div>
        </div>
        <div className="cardP">
          <div className="content">
            <h2 className="title">Bass Guitar</h2>
            <Button className="btn-card" as={Link} to="/products/category/bass">
              Buy Now
            </Button>
          </div>
        </div>
      </div>
    </Fragment>
  );
}
