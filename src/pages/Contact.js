import {
  Form,
  Button,
  Row,
  Col,
  InputGroup,
  FormControl,
} from "react-bootstrap";
import contactImg from "../images/contact.svg";

export default function Contact() {
  return (
    <>
      <div className="circle2"></div>
      <Row className="contactForm">
        <Col className="col-12 col-md-6 order-2">
          <Form className="mx-auto p-5">
            <h1 className="text-white mb-4">Contact Us</h1>

            <Form.Group controlId="Name">
              <Form.Control
                type="text"
                placeholder="Name"
                // value={Name}
                required
              />
            </Form.Group>
            <br />

            <Form.Group controlId="Email">
              <Form.Control
                type="email"
                placeholder="Email"
                // value={Email}
                required
              />
            </Form.Group>
            <br />

            <Form.Group controlId="Phone">
              <Form.Control
                type="text"
                placeholder="Phone Number"
                // value={Email}
                required
              />
            </Form.Group>
            <br />

            <InputGroup>
              <FormControl
                as="textarea"
                aria-label="With textarea"
                placeholder="Message"
              />
            </InputGroup>
            <br />

            <Button
              variant="primary"
              type="submit"
              id="submitBtn"
              className="mt-3 mb-3"
            >
              Submit
            </Button>
          </Form>
        </Col>
        <Col className="col-6 order-1 my-3 my-md-0">
          <img src={contactImg} className="img-fluid shadow-4" alt="..." />
        </Col>
      </Row>
    </>
  );
}
