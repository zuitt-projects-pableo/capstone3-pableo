import React, { useState, useEffect } from "react";
/*react-bootstrap component*/
// import {Container, Button} from 'react-bootstrap'
// import {Link} from 'react-router-dom';
/*components*/
// import product from './../components/product';
import AdminView from "./../components/AdminView.js";

/*mock data*/
// import products from './../mock-data/products';

/*context*/
// import UserContext from './../UserContext';

export default function Admin() {
  const [products, setProducts] = useState([]);

  // const {user} = useContext(UserContext);

  const fetchData = () => {
    fetch("https://backend-capstone3-r6bk.onrender.com/products/all")
      .then((result) => result.json())
      .then((data) => {
        setProducts(data);
      });
  };

  useEffect(() => {
    fetchData();
  }, []);

  return <AdminView productData={products} fetchData={fetchData} />;
}
