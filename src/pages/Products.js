import React, { useState, useEffect } from "react";
import { Container } from "react-bootstrap";
import ProductCard from "../components/ProductCard";
import { useParams } from "react-router-dom";

export default function Products() {
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);
  const { category } = useParams(); // Get the category from the URL

  const fetchAllProducts = () => {
    fetch("https://backend-capstone3-r6bk.onrender.com/products")
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => (
            <ProductCard key={product._id} productProp={product} />
          ))
        );
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching products:", error);
        setLoading(false);
      });
  };

  const fetchProductsByType = () => {
    fetch(
      `https://backend-capstone3-r6bk.onrender.com/products/category/${category}`
    )
      .then((res) => res.json())
      .then((data) => {
        setProducts(
          data.map((product) => (
            <ProductCard key={product._id} productProp={product} />
          ))
        );
        setLoading(false);
      })
      .catch((error) => {
        console.error("Error fetching products by type:", error);
        setLoading(false);
      });
  };

  useEffect(() => {
    setLoading(true);

    if (category) {
      // If a category is in the URL, fetch products by type
      fetchProductsByType();
    } else {
      // If no category in the URL, fetch all products
      fetchAllProducts();
    }
  }, [category]);

  return (
    <>
      <h1 className="text-white pt-5 pb-3 text-capitalize d-flex align-items-center gap-2">
        <span>Products</span>
        {category && (
          <>
            <span className="fs-5">{`/`}</span>
            <span className="fs-5">{`${category} guitars`}</span>
          </>
        )}
      </h1>
      {loading ? (
        <div className="d-flex justify-content-center mt-5">
          <div className="spinner-border text-warning shadow" role="status">
            <span className="visually-hidden">Loading...</span>
          </div>
        </div>
      ) : (
        <Container className="d-flex flex-wrap justify-content-center px-0">
          {products}
        </Container>
      )}
    </>
  );
}
